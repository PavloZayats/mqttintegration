package example.config;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.util.Optional;

@Configuration
public class MqttConfiguration {

    private MQTTProperties mqttProperties;

    @Autowired
    public MqttConfiguration(MQTTProperties mqttProperties) {
        this.mqttProperties = mqttProperties;
    }

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        MqttConnectOptions options = new MqttConnectOptions();
        options.setServerURIs(new String[]{mqttProperties.getConnectionUrl()});
        Optional<String> optionalUsername = Optional.ofNullable(mqttProperties.getUsername());
        optionalUsername.ifPresent(options::setUserName);
        Optional<String> optionalPassword = Optional.ofNullable(mqttProperties.getPassword());
        optionalPassword.ifPresent(password -> options.setPassword(password.toCharArray()));
        options.setAutomaticReconnect(true);
        factory.setConnectionOptions(options);
        return factory;
    }

    @Bean
    public MessageChannel requestChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageProducer inboundRequestHandler() {
        String clientId = MqttAsyncClient.generateClientId();
        MqttPahoMessageDrivenChannelAdapter adapter =
                new MqttPahoMessageDrivenChannelAdapter(clientId,
                        mqttClientFactory(),
                        mqttProperties.getRequestTopic());
        adapter.setCompletionTimeout(5000);
        adapter.setConverter(new DefaultPahoMessageConverter());
        adapter.setQos(0);
        adapter.setOutputChannel(requestChannel());
        return adapter;
    }

    @Bean
    public MessageChannel responseChannel() {
        return new DirectChannel();
    }

    @Bean
    @ServiceActivator(inputChannel = "responseChannel")
    public MessageHandler responseHandler() {
        String clientId = MqttAsyncClient.generateClientId();
        MqttPahoMessageHandler messageHandler =
                new MqttPahoMessageHandler(clientId, mqttClientFactory());
        messageHandler.setAsync(true);
        messageHandler.setDefaultTopic(mqttProperties.getResponseTopic());
        return messageHandler;
    }

}
