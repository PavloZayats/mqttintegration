package example.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@NoArgsConstructor
@Configuration
@ConfigurationProperties("mqtt")
public class MQTTProperties {

    private String connectionUrl;
    private String username;
    private String password;

    private String requestTopic;
    private String responseTopic;

}
