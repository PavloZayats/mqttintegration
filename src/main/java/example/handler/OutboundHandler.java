package example.handler;

import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway(defaultRequestChannel = "responseChannel")
public interface OutboundHandler {

    void sendToMqtt(String data);

}