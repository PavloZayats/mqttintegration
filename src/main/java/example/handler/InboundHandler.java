package example.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
public class InboundHandler implements MessageHandler {

    private OutboundHandler outboundHandler;

    @Autowired
    public InboundHandler(OutboundHandler outboundHandler){
        this.outboundHandler = outboundHandler;
    }

    @Override
    @ServiceActivator(inputChannel = "requestChannel")
    public void handleMessage(Message<?> message) {
        String userName = message.getPayload().toString();
        ZonedDateTime now = ZonedDateTime.now();
        String currentTimeMessage = String.format("Hello %s, the current time is %s", userName, now.toString());
        outboundHandler.sendToMqtt(currentTimeMessage);
    }

}
